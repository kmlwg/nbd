object Main {
  def main(args: Array[String]): Unit = {
    // Zad 01
    println("01:")
    println("\tPoniedziałek => " + zad01("Poniedziałek"))
    println("\tSobota       => " + zad01("Sobota"))
    println("\tXYZ          => " + zad01("XYZ"))

    // Zad 02
    val k1 = new KontoBankowe()
    val k2 = new KontoBankowe(10000)
    println("02: ");
    println("\tDefault => " + k1.getStanKonta)
    println("\t10000   => " + k2.getStanKonta)
    // Zad 03, 04
    val o1 = Osoba("Jan", "Kowalski");
    val o2 = Osoba("Gzegorz", "Brz");
    val o3 = Osoba("Anna", "Borys");
    val o4 = Osoba("Janina", "Motyka");
    println("03: ")
    println("\tBrz    => " + zad03_powitanie(o2))
    println("\tBorys  => " + zad03_powitanie(o3))
    println("\tMotyka => " + zad03_powitanie(o4))
    def plusplus(n: Int):Int = n + 2;
    println("04: Dla 123 => " + zad04(123, plusplus))

    // Zad 05
    val o5 = new Osoba("Janek", "Kowalski") with Nauczyciel;
    val o6 = new Osoba("Jan", "Olszanski") with Student;
    val o7 = new Osoba("Piotr", "Enty") with Pracownik;
    o5.pensja = 5000;
    o7.pensja = 3000;
    println("05:")
    println("\tNauczyciel => Podatek: " + o5._podatek + " => " + o5._podatek * o5.pensja);
    println("\tStudent    => Podatek: " + o6._podatek + " => ");
    println("\tPracownik  => Podatek: " + o7._podatek + " => " + o7._podatek * o7.pensja);

  }
  def zad01(day:String):String={
    val work = List("Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek");
    val free = List("Sobota", "Niedziela");
    day match {
      case a if (work.contains(a)) => "Praca";
      case b if (free.contains(b)) => "Wolne";
      case _ => "Nie ma takiego dnia";
    }
  }
  def zad03_powitanie(o:Osoba):String = {
    o match {
      case a if(o.nazwisko.contains("Brz")) => "Witam " + o.nazwisko;
      case b if(o.nazwisko.contains("Borys")) => "Dzień dobry " + o.imie + o.nazwisko;
      case _ => "Hello " + o.imie;
    }
  }

  def zad04(n:Int, func: (Int) => Int): Int = {
    func(func(func(n)))
  }
}
class KontoBankowe(stanKonta: Double) {
  private var _stanKonta: Double = stanKonta;
  def this() {
    this(0)
  }

  def getStanKonta: Double = _stanKonta;

  def wplata(kwota: Double): Double = {
    _stanKonta += kwota
    _stanKonta
  }

  def wyplata(kwota: Double):Double = {
    if (_stanKonta <= kwota) return _stanKonta;
    _stanKonta -= kwota
    _stanKonta
  }
}
case class Osoba(imie: String, nazwisko: String) {
  val _podatek:Double = 0;
}

trait Student extends Osoba{
  override val _podatek:Double = 0;

}
trait Pracownik extends Osoba {
  override val _podatek:Double = 0.2
  var pensja: Double = 0
}
trait Nauczyciel extends Pracownik {
  override val _podatek: Double = 0.1
}