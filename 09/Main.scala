object Main {
  def main(args: Array[String]) = {
    def plusone(n:Int):Int = {n + 1}
    val c = new Container[Int](2)
    println("01:")
    println(c.getContent())
    println(c.applyFunction[Int](plusone))

    println("\n02: ")
    val y = new Yes[Int](1);
    val n = new No()
    println(y.isInstanceOf[Maybe[_]])
    println(n.isInstanceOf[Maybe[_]])
    println("03: ")
    println("Yes\t" + y.applyFunction(plusone))
    println("No \t" + n.applyFunction(plusone))
    println("04: ")
    println("Yes\t" + y.getOrElse(1))
    println("No \t" + n.getOrElse(1))
  }
}

class Container[A](private val x:A){
  def getContent():A = { x }
  def applyFunction[R](func: A => R): R = {func(getContent())}
}

trait Maybe[A]{}
class No extends Maybe[Nothing]{
  def applyFunction[A,R](f: A => R) : No = {
    new No()
  }
  def getOrElse[A](x: A) = {
    x
  }
}
class Yes[A](var x: A) extends Maybe[A] {
  def applyFunction[R] (fun: A => R) = fun(x)
  def getOrElse(param: A) = x
}