import riak


def addFile(clientBucket, person):
    newPerson = clientBucket.new(person['pesel'], data=person)
    newPerson.store()

def fetchPerson(clientBucket, person):
    fetchedPerson = clientBucket.get(person['pesel'])
    return fetchedPerson.data


def modifyFile(bucket, key):
    fetchedPerson = bucket.get(key).data
    fetchedPerson['contractExpired'] = True
    updatedPerson = bucket.new(fetchedPerson['pesel'], data=fetchedPerson)
    updatedPerson.store()


def deleteFile(bucket, key):
    fetchedPerson = bucket.get(key)
    fetchedPerson.delete()


if __name__ == '__main__':
    f = open('komunikaty.txt', 'w')
    myClient = riak.RiakClient(protocol='http', host='192.168.0.14', http_port=8098)

    person = {
        'pesel': '70032104523',
        'name': 'Jason Bourne',
        'age': '40',
        'contractExpired': False,
    }

    clientBucket = myClient.bucket('clients')

    addFile(clientBucket, person)
    print('Added file:')
    f.write('Added file:\n')
    print(fetchPerson(clientBucket, person))
    f.write(str(fetchPerson(clientBucket, person)) + '\n')
    print('Modified (expiredContract):')
    f.write('Modified (expiredContract):\n')
    modifyFile(clientBucket, person['pesel'])
    print(fetchPerson(clientBucket, person))
    f.write(str(fetchPerson(clientBucket, person))+'\n')
    print('Delete:')
    f.write('Delete:\n')
    deleteFile(clientBucket, person['pesel'])
    print(fetchPerson(clientBucket, person))
    f.write(str(fetchPerson(clientBucket, person))+'\n')
    f.close()




