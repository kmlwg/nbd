// Średnia i łączna ilość środków na kartach kredytowych kobiet narodowości
// polskiej w podziale na waluty.

printjson(
  db.people.aggregate(
    [
      {
        $match : { 
          $and: [
            { sex : "Female" },
            { nationality: "Poland" }
          ]
        }
      },
      {
        $unwind: {path: "$credit"}
      },
      {
        $group: {
          _id: "$credit.currency",
          avg: { "$avg": "$credit.balance"},
          total: { "$sum": "$credit.balance" }
        }  
      }
    ]  
  ).toArray()
)