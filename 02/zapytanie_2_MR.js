// Łączną ilość środków pozostałych na kartach kredytowych osób w bazie, 
// w podziale na waluty;
var mapF = function() {
  for (var i in this.credit) {
    emit(
      this.credit[i].currency,
      this.credit[i].balance
    )
  }
}
  
var reduceF = function(keys, objVals) {
  return Array.sum(objVals);
}
  
db.people.mapReduce(
  mapF,
  reduceF,
  {
    out: "mpr",
  }
)
printjson(db.mpr.find().toArray());