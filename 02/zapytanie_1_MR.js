// Średnią wagę i wzrost osób w bazie z podziałem na płeć (tzn. osobno mężczyzn
// , osobno kobiet); 

var mapF = function() {
  emit(
    this.sex, 
    { 
      count: 1,
      weight: this.weight,
      height: this.height
    }
  )
}

var reduceF = function(keys, objVals) {
  let reducedVal = {
    count: 0,
    avgWeight: 0.0,
    avgHeight: 0.0
  }
  for (let i = 0; i < objVals.length; i++) {
    reducedVal.count += objVals[i].count;
    reducedVal.avgHeight += objVals[i].height;
    reducedVal.avgWeight += objVals[i].weight;
  }
  reducedVal.avgHeight = reducedVal.avgHeight / reducedVal.count;
  reducedVal.avgWeight = reducedVal.avgWeight / reducedVal.count;
  return reducedVal;
}



db.people.mapReduce(
  mapF,
  reduceF,
  {
    out: "mpr",
  }
)

printjson(db.mpr.find().toArray())
