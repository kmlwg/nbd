// Średnia i łączna ilość środków na kartach kredytowych kobiet narodowości
// polskiej w podziale na waluty.

var mapF = function() {
  for (var i in this.credit) {
    emit(
      {
        currenccy: this.credit[i].currency,
        sex: this.sex,
        nationality: this.nationality
      },
      {
        
        balance: this.credit[i].balance
      }
    )
  }
}
    
var reduceF = function(keys, objVals) {
  var reducedVal = {
    avgBalance: 0.0,
    totalBalance: 0.0
  };

  for (i in objVals) {
    reducedVal.totalBalance += objVals[i].balance;
  }
  
  reducedVal.avgBalance = reducedVal.totalBalance / objVals.length;
  
  return  reducedVal;
}
    
db.people.mapReduce(
  mapF,
  reduceF,
  {
    out: "mpr",
    query: {"$and": [{"sex": "Female"}, {"nationality": "Poland"}]}
  }
)
printjson(db.mpr.find().toArray());