// Łączną ilość środków pozostałych na kartach kredytowych osób w bazie, 
// w podziale na waluty;

printjson(
  db.people.aggregate(
    [
      {
        $unwind: {path: "$credit"}
      },
      {
        $group: {
          _id: "$credit.currency",
          total: {$sum: "$credit.balance"}
        }
      }
    ]
  ).toArray()
)