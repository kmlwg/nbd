// Średnie, minimalne i maksymalne BMI (waga/wzrost^2) dla osób w bazie, 
// w podziale na narodowości; 

printjson(
  db.people.aggregate(
    [
      {
        $addFields: {
          heightM: {
            "$divide": [
              "$height",
              100.0
            ]
          }
        }
      },
      {
        $project: {
          bmi: {
            "$divide": [
              "$weight",
              {"$multiply": [
                "$heightM",
                "$heightM"
              ]}
          ]},
          nationality: 1
        }
      },
      {  
        $group: {
          _id: "$nationality",
          avgBMI: { "$avg": "$bmi"}
        }
      }
    ]
  ).toArray()
)