// Średnie, minimalne i maksymalne BMI (waga/wzrost^2) dla osób w bazie, 
// w podziale na narodowości; 
var mapF = function() {
      emit(
        this.nationality,
        {
          count: this.count,
          height: this.height,
          weight: this.weight,
        }
      )

  }
    
  var reduceF = function(keys, objVals) {
    var bmi = 0.0
    for (var i in objVals) {
      var height = objVals[i].height / 100.0;
      bmi += objVals[i].weight / (height * height);
    }
    return bmi / objVals.length

  }
    
  db.people.mapReduce(
    mapF,
    reduceF,
    {
      out: "mpr",
    }
  )
  printjson(db.mpr.find().toArray());