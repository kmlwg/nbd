import scala.annotation.tailrec

object Main {
  def main(args: Array[String]) = {
    val week = List("Pon", "Wt", "Sr", "Czw", "Pt", "Sb", "Nd")
    println("01a: " + zad01a(week));
    println("01b: " + zad01b(week));
    println("01c: " + zad01c(week));
    println("02a: " + zad02a(week));
    println("02b: " + zad02b(week));
    println("03 : " + zad03(week));
    println("04a: " + zad04a(week));
    println("04b: " + zad04b(week));
    println("04c: " + zad04c(week));
    println("05 : ")
    zad05()
    println("06 : ")
    zad06((3.14, 100, "XYZ"))
    println("07 : " + zad07())
    val numbers = List(-12, 0, -3, -1, 0, 10, 45, 123, 123123);
    println("08 :")
    println("\tOriginal> " + numbers)
    println("\t     New> " + zad08(numbers))
    println("09 :")
    println("\tOriginal> " + numbers)
    println("\t     New> " + zad09(numbers))
    println("10 :")
    println("\tOriginal> " + numbers)
    println("\t     New> " + zad10(numbers))
  }

  def zad01a(week: List[String]):String ={
    var finalStr = "";

    for (d <- week) { finalStr += d + ", " }
    finalStr = finalStr.dropRight(2);
    finalStr
  }
  def zad01b(week: List[String]):String ={
    var finalStr = "";

    for (d <- week if d.startsWith("P")) { finalStr += d + ", " }
    finalStr = finalStr.dropRight(2);
    finalStr;
  }
  def zad01c(week: List[String]):String= {
    var finalStr = "";
    var i = 0;
    while (i < week.length) {
      finalStr += week(i) + ", "; i += 1
    }
    finalStr = finalStr.dropRight(2)
    finalStr;
  }
  def zad02a(week: List[String]):String = {
    def listToString(week: List[String], n:Int): String = {
      if (n == week.length) ""
      else {
        val nextDay = if (n==week.length - 1) week(n) else week(n) + ", "
        nextDay + listToString(week, n+1)
      }
    }
    listToString(week, 0)
  }
  def zad02b(week: List[String]):String = {
    def listToString(week: List[String], n:Int): String = {
      if (n == -1) ""
      else {
        val nextDay = if (n == 0) week(n) else week(n) + ", "
        nextDay + listToString(week, n - 1)
      }
    }
    listToString(week, week.length - 1)
  }
  def zad03(week: List[String]):String = {
    @tailrec
    def addDay(week: List[String], i:Int, finalStr: String):String = {
      if (i == week.length) return finalStr;
      val d = if (i == week.length - 1) week(i) else week(i) + ", ";
      addDay(week, i + 1, finalStr + d);
    }
    addDay(week, 0, "")
  }
  def zad04a(week: List[String]):String = {
    week.fold("") {(sum, cur) => {
      sum + cur + ", "
    }}.dropRight(2)
  }
  def zad04b(week: List[String]):String = {
    week.foldRight(""){ (sum, cur) => {
      sum + ", " + cur
    }}.dropRight(2)
  }
  def zad04c(week: List[String]):String = {
    week.fold("") {(sum, cur) => {
      if (cur.startsWith("P")) sum + cur + ", "
      else sum
    }}.dropRight(2)
  }
  def zad05()={
    val prods = Map("Woda" -> 2, "Drob" -> 13, "Pomidory" -> 10)
    val prodsRed = prods map{ case (k, v) => (k, v * 0.9)}
    println("\tOriginal> " + prods)
    println("\tReduced > " + prodsRed)
  }
  def zad06(tup: (Double, Int, String)) = {
    println("\t> " + tup._1)
    println("\t> " + tup._2)
    println("\t> " + tup._3)
  }
  def zad07():Option[Int] = {
    val prods = Map("Woda" -> 2, "Drob" -> 13, "Pomidory" -> 10)
    return prods.get("Woda")
  }
  def zad08(nums: List[Int]):List[Int] = {
    def next(i: Int, newNums: List[Int]):List[Int] = {
      if (i >= newNums.length) return newNums;
      val (l,r) = newNums.splitAt(i);
      if (newNums(i) == 0) next(i + 1, l ++ r.tail)
      else next(i + 1, newNums)
    }
    next(0, nums)
  }
  def zad09(nums: List[Int]):List[Int] = {
    nums map (x => x + 1)
  }
  def zad10(nums: List[Int]):List[Int] = {
    val fNums  = nums filter(x => x >= -5 && x <= 12)
    fNums map (x => x.abs)
  }
}
