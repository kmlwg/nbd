printjson(db.people.insertOne(
	{
		"sex": "Male",
		"first_name" : "Kamil",
		"last_name": "Giedrys",
		"job": "Student",
		"email": "kg@google.pl",
		"location": {
			"city" : "Warsaw",
			"address": {
				"streetname" : "Wilcza",
				"streetnumber": "999"
			}
		},
		"description": "lorem ipsum",
		"height": "175",
		"weight": "67.25",
		"birthdate": "1996-01-01T15:30:30Z",
		"nationality": "Poland",
		"credit": {
			"type": "bbb",
			"number": "12344324543265",
			"currency": "PLN",
			"balance": "9292.01"
		}
	}
))